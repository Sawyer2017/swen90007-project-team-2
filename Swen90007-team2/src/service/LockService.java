package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import model.Page;

public class LockService {
	private Connection conn;
	private PreparedStatement pstmt;
	public LockService() {
		conn = new connection.Conn().getConn();
	}
	public boolean hasLock(int id){
		try {
			pstmt = conn.prepareStatement("select * from locktable where locktableid=?");
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
		    return true;
			}
			return false;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	public boolean acquireLock(int id) {
		try {
			if(!hasLock(id)){
			pstmt = conn.prepareStatement("insert into locktable "
					+ "(locktableid) "
					+ "VALUES(?)");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
			return true;
			}
			else
				return false;
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}

	}
	public boolean releaseLock(int id) {
		try {
			pstmt = conn.prepareStatement("delete from  locktable where locktableid=?");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
			return true;
		} catch (Exception e) {

			e.printStackTrace();
			return false;
		}

	}
}
