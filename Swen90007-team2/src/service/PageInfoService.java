package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import model.Page;

public class PageInfoService {
	private Connection conn;
	private PreparedStatement pstmt;
	public PageInfoService() {
		conn = new connection.Conn().getConn();
	}
	public boolean addPage(Page page) {
		try {
			pstmt = conn.prepareStatement("insert into page"
					+ "(title,content,author) "
					+ "values(?,?,?)");
			pstmt.setString(1, page.getTitle());
			pstmt.setString(2, page.getContent());
			pstmt.setString(3, page.getAuthor());
			pstmt.executeUpdate();
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block

			e.printStackTrace();
			return false;
		}

	}
	public boolean deletePage(int id){
		try {
			pstmt = conn.prepareStatement("delete from  page where id=?");
			pstmt.setInt(1, id);
			pstmt.executeUpdate();
			return true;
		} catch (Exception e) {
			e.getStackTrace();
			return false;
		}
	}
	public boolean updatePage (Page page){
		try {
			pstmt = conn
					.prepareStatement("update  page set title=? , content=?  "
							+ "    where id=?");
			pstmt.setString(1, page.getTitle());
			pstmt.setString(2, page.getContent());
			pstmt.setInt(3, page.getId());
			pstmt.executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
		
	
	public Page queryPagebyID(int id){
		try {
			pstmt = conn.prepareStatement("select * from page where id=?");
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();
			if (rs.next()) {
				Page page = new Page();
				page.setId(rs.getInt(1));
				page.setTitle(rs.getString(2));
				page.setContent(rs.getString(3));
				return page;

			}
			return null;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	public ArrayList<Page> queryAllPage() {
		ArrayList<Page> pages = new ArrayList<Page>();
		try {
			pstmt = conn.prepareStatement("select * from page");
			ResultSet rs = pstmt.executeQuery();
			while (rs.next()) {
				Page page = new Page();
				page.setId(rs.getInt(1));
				page.setTitle(rs.getString(2));
				page.setContent(rs.getString(3));
				page.setAuthor(rs.getString(4));
				pages.add(page);

			}
			return pages;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
}
