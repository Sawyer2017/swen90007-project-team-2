package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import model.Page;
import model.User;

public class UserService {
	private Connection conn;
	private PreparedStatement pstmt;
	public UserService() {
		conn = new connection.Conn().getConn();
	}
	public User validate (User user) {
		try {
		pstmt=conn.prepareStatement("select * from user where username=? and password=?");
		pstmt.setString(1,user.getUsername());
		pstmt.setString(2,user.getPassword());
		ResultSet rs=pstmt.executeQuery();
		if (rs.next()) {
			User user1 = new User();
			user1.setUsername(rs.getString(1));
			user1.setPassword(rs.getString(2));
			user1.setUsertype(rs.getInt(3));
			return user1;
		}
		else
			return null;
		
		
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
