package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Page;
import model.User;
import service.PageInfoService;



public class InputPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public InputPageServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		HttpSession session=request.getSession();
		User user=(User)session.getAttribute("user");
		String title=request.getParameter("title");
		String content=request.getParameter("content");
		String author=user.getUsername();
		Page page=new Page();
		page.setTitle(title);
		page.setContent(content);
		page.setAuthor(author);
		
		if(new PageInfoService().addPage(page))
			response.sendRedirect("../inputPage_success.jsp");
		else
			response.sendRedirect("../inputPage.jsp");
	}

}
