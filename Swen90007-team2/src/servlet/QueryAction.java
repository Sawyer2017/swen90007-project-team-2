package servlet;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Page;
import service.PageInfoService;

public class QueryAction extends HttpServlet {
	private static final long serialVersionUID = 1L;
       

    public QueryAction() {


    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
           doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String osid=request.getParameter("osid");
		ArrayList pages=new PageInfoService().queryAllPage();
		HttpSession session=request.getSession();
		session.setAttribute("pages",pages);
		if(osid.equals("query"))
			response.sendRedirect("../displayPageInfo.jsp");
		else if(osid.equals("modify"))
			response.sendRedirect("../modifyPageInfo.jsp");
		else
			response.sendRedirect("../deletePageInfo.jsp");
	}

}
