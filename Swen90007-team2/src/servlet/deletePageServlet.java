package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import service.PageInfoService;

public class deletePageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public deletePageServlet() {
        super();
        
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=Integer.parseInt(request.getParameter("id"));
		if(new PageInfoService().deletePage(id))
			response.sendRedirect("../deletePage_success.jsp");
		else
			response.sendRedirect("../deletePageInfo.jsp");
	}

}
