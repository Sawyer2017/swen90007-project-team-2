package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.websocket.Session;

import model.User;
import service.PageInfoService;
import service.UserService;


public class Validate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public Validate() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("utf-8");
		String username=request.getParameter("username");
		String password=request.getParameter("password");
		User user=new User();
		user.setUsername(username);
		user.setPassword(password);
		UserService us= new UserService();
		User user1=us.validate(user);
		if( user1!=null){
			HttpSession session=request.getSession();	
		    session.setAttribute("user", user1);
			response.sendRedirect("../main.jsp");
		}
		else
			response.sendRedirect("../index.jsp");
	}

}
