package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Page;
import service.LockService;
import service.PageInfoService;

public class modifyOnePageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public modifyOnePageServlet() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id=Integer.parseInt(request.getParameter("id"));
		if(new LockService().acquireLock(id)){
		Page page=new PageInfoService().queryPagebyID(id);
		HttpSession session=request.getSession();
		session.setAttribute("page",page);
		response.sendRedirect("../modifyOnePage.jsp");
		}
		else{
			response.sendRedirect("../nomodify.jsp");
		}
	}

}
