package servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Page;
import service.LockService;
import service.PageInfoService;


public class modifyOnePageInfoServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public modifyOnePageInfoServlet() {
        super();

    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
     doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		int id=Integer.parseInt(request.getParameter("id"));
		
		String title=request.getParameter("title");
		String content=request.getParameter("content");
		
		Page page=new Page();
		page.setId(id);
		page.setTitle(title);
		page.setContent(content);
		if(new PageInfoService().updatePage(page)){
			new LockService().releaseLock(id);
			response.sendRedirect("../modifyOnePage_success.jsp");
			
		}
		else
			response.sendRedirect("../modifyOnePage.jsp");
	}

}
