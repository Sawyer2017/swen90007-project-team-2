<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" import="java.util.*" import="model.Page" import="model.User"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<%
		User user=(User) session.getAttribute("user"); 
        String username=user.getUsername();
        ArrayList pages =(ArrayList) session.getAttribute("pages"); 
		Iterator iter = pages.iterator();
	%>
<table>
		<tr>
			<td>id</td>
			<td>title</td>
			<td>content</td>
		</tr>
		<%
			int i = 0;
		if(user.getUsertype()==0) {
			while (iter.hasNext()) {
				Page page1 = (Page)iter.next();
				
%>
<tr <%if (i % 2 == 0) {%> bgcolor="#F0F8FF" <%}%>>
	<td><%=page1.getId()%></td>
	<td><%=page1.getTitle()%></td>
	<td><%=page1.getContent()%></td>
</tr>
<%
	i++;
		}
		}
				if(user.getUsertype()==1){
					while (iter.hasNext()) {
						Page page1 = (Page)iter.next();
						if(username.equals(page1.getAuthor())){
		%>
       <tr <%if (i % 2 == 0) {%> bgcolor="#F0F8FF" <%}%>>
			<td><%=page1.getId()%></td>
			<td><%=page1.getTitle()%></td>
			<td><%=page1.getContent()%></td>
		</tr>
		<%
			i++;
				}
					}
				}
				%>
	</table>
			
</body>
</html>